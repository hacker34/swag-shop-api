/**
 * Created by hacker on 4/23/17.
 */
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var routes = require('./routes');
var mongoose = require('mongoose');
// Code to connect to our database.
var db = mongoose.connect('mongodb://localhost/swag-shop');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// look at routes file to figure out APIs
app.use('/api', routes);

app.listen(3004, function () {
   console.log("Swag Shop API running on port 3004...");
});
