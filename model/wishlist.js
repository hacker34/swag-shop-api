/**
 * Created by hacker on 4/23/17.
 */
// product data model
var mongoose = require('mongoose');
// Schema is like blueprints for a house to be build
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;

var wishList = new Schema({
    title: {type: String, default: "Cool Wish List"},
    // This is referencing an Object in the Product Schema (relationship)
    products:[{type: ObjectId, ref:'Product'}]
});

module.exports = mongoose.model('WishList', wishList);