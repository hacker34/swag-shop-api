/**
 * Created by hacker on 4/23/17.
 */
// product data model
var mongoose = require('mongoose');
// Schema is like blueprints for a house to be build
var Schema = mongoose.Schema;

var product = new Schema({
    title: String,
    price: Number,
    likes: {type: Number, default:0}
});

// This line of code is what exports or inserts into our mongo database.
module.exports = mongoose.model('Product', product);