/**
 * Created by jhacking on 4/28/2017.
 */
var mongoose = require('mongoose');

// Bringing in our database Schemas
var Product = require('./../model/product');
// Saving a new product
module.exports = {
  postProduct : function(request, response) {
      var product = new Product();
      product.title = request.body.title;
      product.price = request.body.price;
      product.save(function(err, savedProduct) {
          if (err){
              response.status(500).send({error:"Could not save product"});
          } else {
              response.send(savedProduct);
          }
      });
  },
    // Getting a products to show
    getProduct : function(request, response) {
        // on the find remember the brackets are fetching an array of objects so yo have to get the first item to dig
        // into that array.
        Product.find({}, function (err, products) {
            if (err) {
                response.status(500).send({error: "couldn't find any products"});
            } else {
                response.send(products);
            }
        });
    }
};

