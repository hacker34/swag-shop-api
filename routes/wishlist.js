/**
 * Created by jhacking on 4/28/2017.
 */
var mongoose = require('mongoose');

var Product = require('./../model/product');
var WishList = require('./../model/wishlist');

module.exports = {
    postWishList: function (request, response) {
        var wishList = new WishList();
        wishList.title = request.body.title;

        wishList.save(function (err, newWishList) {
            if (err) {
                response.status(500).send({error: "Could not save Wish list."});
            } else {
                response.send(newWishList);
            }
        })
    },
    putWishList: function(request, response) {
        Product.findOne({_id: request.body.productId}, function(err, product) {
            if(err){
                response.status(500).send({error: "Could not add item to wish list"});
            }else{
                WishList.update({_id:request.body.wishListId}, {$addToSet:{products: product._id}}, function (err, wishList) {
                    if(err){
                        response.status(500).send({error:"Could not add item to wish list"});
                    }else{
                        response.send("Item added to Wish List!");
                    }
                });
            }
        })
    },
    getWishList: function (request,response) {
        WishList.find({}).populate({path:'products', model: 'Product'}).exec(function (err, wishLists) {
            if(err){
                response.status(500).send({error: "Didn't find anything;"});
            }else{
                response.send(wishLists);
            }
        })
    },
    getOneWishList: function (request,response) {
        WishList.findOne({_id:request.body.wishListId}).populate({path:'products', model:'Product'}).exec( function (err, wishlist) {
            if(err){
                response.status(500).send({error: "No Wish List found"});
            }else{
                response.send(wishlist);
            }

        })
    }

};