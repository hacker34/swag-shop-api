/**
 * Created by jhacking on 4/28/2017.
 */
// Post API to insert product
var express = require('express');
var productCtrl = require('./routes/product');
var wishlistCtrl = require('./routes/wishlist');

var router = express.Router();
// Routes for API
router.route('/product').post(productCtrl.postProduct);
router.route('/product').get(productCtrl.getProduct);
router.route('/wishlist').post(wishlistCtrl.postWishList);
router.route('/wishlist').get(wishlistCtrl.getWishList);
router.route('/wishlist/product/add').put(wishlistCtrl.putWishList);
router.route('/wishlistOne').post(wishlistCtrl.getOneWishList);

module.exports = router;